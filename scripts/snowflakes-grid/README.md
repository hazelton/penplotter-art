# snowflakes-grid

*this is a work in progress*

The python script utilizes [Mecode](https://github.com/jminardi/mecode) to simplify GCODE generation.

To generate the snowflakes grid:

    $ python3 snowflakes-grid.py
    
The gcode generated will overwrite the existing gcode file:  

    snowflakes-grid.py.gcode

Here's the gcode rendered before plotting:

![an array of stylized snowflakes rendered gcode](snowflakes-grid.png)

