"""
This code generates a matrix of stylized snowflakes constructed from a randomized set of smaller elements.
A gcode output file is created using mecode, which may be suitable for use with a penplotter running GRBL.
Copyright 2022, hazelton

"""
from mecode import GMatrix
from random import seed, randrange, choice, uniform
from math import pi, cos

GCODE_OUTFILE = "{}.gcode".format(__file__)

# pen options
FEEDRATE = 1000
PEN_Z_FEEDRATE = 400
PEN_UP_Z = 5
PEN_DOWN_Z = -1

# printer bounds in mm
PRINTER_MAX_X = 100
PRINTER_MAX_Y = 100

# border outline offset
BORDER_OFFSET = 5

# define pattern matrix
COLUMNS = 5
ROWS = 5

# pattern options
# flake radius (rough)
FLAKE_MAX = 5.5
FLAKE_MIN = 4
DOTS_OFFSET = 1.2 

# random seed, set to reproduce the same flakes
# seed(1111)

# pen handlers
def pen_up():
  g.abs_move(z=PEN_UP_Z, F=PEN_Z_FEEDRATE, rapid=True)
  
def pen_down():
  g.abs_move(z=PEN_DOWN_Z, F=PEN_Z_FEEDRATE, rapid=True)
  
# draw hexagon at current position
# starting at center
# size is outer radius 
def draw_hexagon(size): 
  # move to edge
  g.move(-size, 0, F=FEEDRATE, rapid=True)  
  
  pen_down()
  g.move(0.5 * size, cos(pi/6) * size, F=FEEDRATE)  
  g.move(1 * size, 0, F=FEEDRATE)  
  g.move(0.5 * size, -cos(pi/6) * size, F=FEEDRATE)  
  g.move(-0.5 * size, -cos(pi/6) * size, F=FEEDRATE)  
  g.move(-1.0 * size, 0, F=FEEDRATE)  
  g.move(-0.5 * size, cos(pi/6) * size, F=FEEDRATE) 
  pen_up()
  
  # move to center
  g.move(size, 0, F=FEEDRATE, rapid=True)
  
# draw decoration at current position
# starting at left center
# size is outer radius 
# optional dots on top, right, bottom
# optional offset from center
def hex_decoration(size, dots = False, offset = 0):
  
  # move to offset
  g.move(offset, 0, F=FEEDRATE, rapid=True)  
  
  # hexagon decoration
  draw_hexagon(size)
  
  # move to center
  g.move(-offset, 0, F=FEEDRATE, rapid=True)
  
  if dots:
    # move to dot location 1 
    g.move(1 * size, DOTS_OFFSET * size, F=FEEDRATE, rapid=True) 
    draw_hexagon(0.1 * size)
    # move to dot location 2 
    g.move(DOTS_OFFSET * size, -DOTS_OFFSET * size, F=FEEDRATE, rapid=True)  
    draw_hexagon(0.1 * size)
    # move to dot location 3 
    g.move(-DOTS_OFFSET * size, -DOTS_OFFSET * size, F=FEEDRATE, rapid=True)  
    draw_hexagon(0.1 * size)
    # move back to original location 
    g.move(-1 * size, DOTS_OFFSET * size, F=FEEDRATE, rapid=True) 
    

  
# draws a snowflake at the specified position
# size in mm (roughly outer radius)
# optional 30 degree rotation
# note: there's quite a bit of extraneous travel as elements are drawn
def snow_flake(x, y, size, flake_rotation = False):
  # set some random feature sizes/options
  
  # squiggly stalk flag 
  squiggly_stalk = randrange(0,5)
  
  # sprigs count
  sprig_count = randrange(2, int(size) + 2)
  
  # sprig adjust length
  sprig_adjust = round(uniform(0.5, 1.2), 4)
  
  # sprig adjust angle
  sprig_angle = round(uniform(-0.8, 1.1), 1)
  
  # sprig spacing
  sprig_spacing = round(uniform(0.5, 1.3), 4)

  # outer hexagon decoration sizes and offsets
  hex_decoration_size = [
    round(uniform(0, 0.8), 4), 
    round(uniform(0.2, 1.2), 4), 
    round(uniform(1.0, 1.5), 4)
    ]
  hex_decoration_offset = [
    1, 
    round(uniform(-0.9, 1.2), 4), 
    round(uniform(0.8, 1.4), 4)
    ]
  # skip largest hexagon sometimes
  if choice([0, 1]):
    hex_decoration_size[2] = 0
  
  # bump out from center a bit
  center_hex_size = round(uniform(0, 2), 3)
    
  # move to absolute x,y position, pen up!
  pen_up()
  g.abs_move(x, y, F=FEEDRATE, rapid=True)
  
  # flake rotation, 30 degrees
  if flake_rotation:
    g.push_matrix()
    g.rotate(pi/6)
 
  # add center hexagon
  draw_hexagon(center_hex_size)
    
  # optional large outer hexagon
  if choice([0, 0, 1]):
    draw_hexagon(size)

  # optional second large outer hexagon
  if choice([1, 0, 0, 0]):
    draw_hexagon(size + hex_decoration_size[1])
  
  if flake_rotation:
    # main flake unrotate
    g.pop_matrix()
      
      
  # cycle thru 60 degree angles, repeating 6 flake stalk doodles
  rotation = 0  
  while rotation <= (5 * pi / 3) :
    # move to center origin
    g.abs_move(x, y, F=FEEDRATE, rapid=True)
    
    if flake_rotation:
      # flake rotation
      g.push_matrix()
      g.rotate(pi/6)
    
    # stalk rotation
    g.push_matrix()
    g.rotate(rotation)     

    # bump out to center hexagon border
    g.move(center_hex_size, 0, F=FEEDRATE, rapid=True)
    
    # draw stalk
    # optionally make it squiggley
    if squiggly_stalk == 1:
      pen_down()
      g.push_matrix()
      g.rotate(-pi/2)  
      # store x value to return to after the meander
      saved_x = g.current_position['x']
      g.move(-0.1, 0, F=FEEDRATE, rapid=True)
      g.meander(0.2, size - center_hex_size, spacing=0.2)
      # return to saved_x
      g.abs_move(x=saved_x, F=FEEDRATE, rapid=True)
      g.pop_matrix()
      pen_up()
    else:
      pen_down()
      g.move(size - center_hex_size, 0, F=FEEDRATE)
      pen_up()
   
    # tip decorations at end of stalk
    hex_decoration(hex_decoration_size[0], offset = hex_decoration_size[0] * hex_decoration_offset[0])
    hex_decoration(hex_decoration_size[1], offset = hex_decoration_size[1] * hex_decoration_offset[1])
    # optional third decoration
    if hex_decoration_size[2]:
      hex_decoration(hex_decoration_size[2], dots=True, offset = hex_decoration_size[2] * hex_decoration_offset[2])
    
      
    # move back to center origin
    g.move(-size, 0, F=FEEDRATE, rapid=True)
    
    #sprigs, start at center hexagon 
    sprig_x = center_hex_size * 1.2
    while sprig_x <= sprig_count:
      sprig_half_length = 0.5 * sprig_x * sprig_adjust
      
      # move to start pos
      g.move(sprig_x + sprig_angle, sprig_half_length, F=FEEDRATE, rapid=True)

      # draw sprig
      pen_down()
      g.move(-sprig_angle, sprig_half_length * -1, F=FEEDRATE)
      g.move(sprig_angle, sprig_half_length * -1, F=FEEDRATE)
      pen_up()
      
      # return to stalk Y pos
      g.move(-sprig_x - sprig_angle, sprig_half_length, F=FEEDRATE, rapid=True)
      
      # move up the stalk, for next sprig
      sprig_x = sprig_x + sprig_spacing
    

    # unrotate stalk
    g.pop_matrix() 

    # rotate for next sprig, 60 dgrees
    rotation = rotation + (pi / 3)
  
    if flake_rotation:
      # main flake unrotate
      g.pop_matrix()
    

# main script
# create a mecode instance
# with transform matrix functionality
g = GMatrix(outfile = GCODE_OUTFILE,
  header="header.gcode",
  footer="footer.gcode")
  
# pen up first
pen_up()

# go to printer origin
g.abs_move(0, 0, F=FEEDRATE, rapid=True)  

# go to border origin
g.abs_move(BORDER_OFFSET, BORDER_OFFSET, F=FEEDRATE, rapid=True)

# draw border
pen_down()  
g.abs_move(PRINTER_MAX_X - BORDER_OFFSET, BORDER_OFFSET, F=FEEDRATE) 
g.abs_move(PRINTER_MAX_X - BORDER_OFFSET, PRINTER_MAX_Y - BORDER_OFFSET, F=FEEDRATE) 
g.abs_move(BORDER_OFFSET, PRINTER_MAX_Y - BORDER_OFFSET, F=FEEDRATE) 
g.abs_move(BORDER_OFFSET, BORDER_OFFSET, F=FEEDRATE) 
pen_up()

# flag to alternate flake rotations
flake_rotate = False

# matrix structure
spacing_x = int( (PRINTER_MAX_X - (2 * BORDER_OFFSET)) / (COLUMNS + 1) )
start_x = BORDER_OFFSET + spacing_x 
end_x = int(PRINTER_MAX_X - BORDER_OFFSET - FLAKE_MAX)

spacing_y = int( (PRINTER_MAX_Y - (2 * BORDER_OFFSET)) / (ROWS + 1) )
start_y = BORDER_OFFSET + spacing_y 
end_y = int(PRINTER_MAX_Y - BORDER_OFFSET - FLAKE_MAX)

for flake_x in range(start_x, end_x, spacing_x):
  for flake_y in range(start_y, end_y, spacing_y):
    flake_size = round(uniform(FLAKE_MIN, FLAKE_MAX), 3)
    snow_flake(flake_x, flake_y, flake_size, flake_rotate)
    flake_rotate = not flake_rotate
    
pen_up()
g.teardown()

print("Output file: %s" % GCODE_OUTFILE)

print("Done.")            
