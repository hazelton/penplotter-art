# hershey-simplex

*this is a work in progress*

The python script utilizes [Mecode](https://github.com/jminardi/mecode) to simplify GCODE generation.

To generate gcode for ascii text strings using a hershey vector inspired font:

    $ python3 hershey-simplex.py
    
The gcode generated will overwrite the existing gcode file:  

    hershey-simplex.py.gcode

Here's the gcode after plotting:

![an example of text plotted with the generated gcode](hershey-simplex.png)

